package com.tta.technical_test.data.net

import com.tta.technical_test.data.PokemonDetailApiModel
import com.tta.technical_test.data.PokemonMinApiResult
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query
import javax.inject.Inject
import javax.inject.Singleton

interface PokeApi {
    @GET("pokemon")
    suspend fun pokemonList(
        @Query("limit") limit: Int,
        @Query("offset") offset: Int,
    ): Response<PokemonMinApiResult>

    @GET("pokemon/{name}")
    suspend fun pokemonDetail(@Path("name") name:String): Response<PokemonDetailApiModel>
}


@Singleton
class PokeApiService
@Inject constructor(retrofit: Retrofit) : PokeApi {
    private val moviesApi by lazy { retrofit.create(PokeApi::class.java) }
    override suspend fun pokemonList(limit: Int, offset: Int) = moviesApi.pokemonList(limit,offset)

    override suspend fun pokemonDetail(name: String) = moviesApi.pokemonDetail(name)

}