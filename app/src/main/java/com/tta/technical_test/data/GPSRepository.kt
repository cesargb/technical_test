package com.tta.technical_test.data

import com.tta.technical_test.data.di_modules.GpsDbModule
import javax.inject.Inject
import com.tta.technical_test.data.gps.GpsLocation
import com.tta.technical_test.data.gps.LocationService
import com.tta.technical_test.data.net.GpsApi
import kotlinx.coroutines.flow.Flow


interface GPSDataSource {
    suspend fun  startGetUpdates(): Flow<GpsLocation>
    suspend fun  stopGpsUpdates()
    suspend fun  saveGpsPoint(point:GpsLocation)
    suspend fun  getGpsPoints(): Flow<List<GpsLocation>>
}

class GPSRepository @Inject constructor(
    private val locationService:LocationService,
    private val gpsApi: GpsApi,
    ):GPSDataSource{
    override suspend fun startGetUpdates(): Flow<GpsLocation>  {
        return locationService.getGpsUpdates()
    }
    override suspend fun stopGpsUpdates(){
         locationService.stopGpsUpdates()
    }
    override suspend fun getGpsPoints(): Flow<List<GpsLocation>> {
        return gpsApi.getPoints()
    }

    override suspend fun saveGpsPoint(point:GpsLocation) {
        gpsApi.saveGpsPoint(point)
    }


}

