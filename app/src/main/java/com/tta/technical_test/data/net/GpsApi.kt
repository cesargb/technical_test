package com.tta.technical_test.data.net

import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.ListenerRegistration
import com.google.firebase.installations.FirebaseInstallations
import com.google.type.DateTime
import com.tta.technical_test.data.gps.GpsLocation
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow
import retrofit2.Retrofit
import java.util.*
import javax.inject.Inject
import javax.inject.Singleton
import kotlin.random.Random

interface GpsApi {
    suspend fun saveGpsPoint(point: GpsLocation)
    suspend fun getPoints(): Flow<List<GpsLocation>>
}

@Singleton
class FireApiService
@Inject constructor(private val firestore: FirebaseFirestore) : GpsApi {
    var id:String? = null

    override suspend fun saveGpsPoint(point: GpsLocation){
        withId {
            firestore.collection("disp")
                .document(it)
                .collection("loc")
                .add(hashMapOf(
                    "lat" to point.lat,
                    "lon" to point.lon,
                    "time" to System.currentTimeMillis(),

                ))
        }
    }
    override suspend fun getPoints(): Flow<List<GpsLocation>> = callbackFlow {
        var subscription: ListenerRegistration? = null
        withId {
            subscription = firestore.collection("disp")
            .document(it)
                .collection("loc")
                .addSnapshotListener() { snap,e ->
                    if (snap == null || snap.isEmpty) return@addSnapshotListener
                    trySend(snap.map {  doc->
                        GpsLocation(
                        lon =  doc.data["lon"] as Double,
                        lat =  doc.data["lat"] as Double,
                        time =  doc.data["time"] as Long,

                    ) })
                }


        }
        awaitClose {  subscription?.remove() }
    }


    fun withId(f:(id:String)->Unit){
        val idTxt = id
        if(idTxt!= null) {
            f(idTxt)
            return
        }

        FirebaseInstallations.getInstance().getToken(false)
            .addOnCompleteListener { task ->
                val idTxt = if (task.isSuccessful) {
                    task.result.token
                }else{
                    UUID.randomUUID().toString() //Solo para prueba
                }
                id=idTxt
                f(idTxt)
            }
    }

}