package com.tta.technical_test.data.db

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = [
    PokemonDetaiModel::class,
    PokemonTypeModel::class,
    PokemonFavorite::class
                     ], version = 1)
abstract class PokemonDatabase: RoomDatabase() {
    abstract fun getPokemonDao():PokemonDao
}