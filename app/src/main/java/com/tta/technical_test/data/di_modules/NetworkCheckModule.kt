package com.tta.technical_test.data.di_modules

import android.content.Context
import android.content.Context.CONNECTIVITY_SERVICE
import android.net.ConnectivityManager
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Inject


@Module
@InstallIn(SingletonComponent::class)
class NetworkCheckModule @Inject constructor(@ApplicationContext private val context: Context) {
    fun isConnected():Boolean{
        val manager = context.getSystemService(CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkCapabilities =  manager.getNetworkCapabilities(manager.activeNetwork)
        return networkCapabilities!=null
    }
}