package com.tta.technical_test.data.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface PokemonDao {

    @Query("SELECT * FROM pokemon LIMIT :limit OFFSET :offset")
    suspend fun pokemonList(
        limit: Int,
        offset: Int,
    ): List<PokemonDetaiModel>

    @Query("SELECT * FROM pokemon_types WHERE pokemon_id = :id")
    suspend fun pokemonTypes(
        id:Int
    ): List<PokemonTypeModel>

    @Query("SELECT * FROM pokemon  LIMIT :limit OFFSET :offset")
    suspend fun pokemonWithExtrasList(
        limit: Int,
        offset: Int,
    ): List<PokemonWExtras>

    @Query("SELECT * FROM pokemon where id= :id")
    suspend fun pokemonById(
        id: Int,
    ): PokemonWExtras?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertPokemonWithTypes(pokemon: List<PokemonDetaiModel>,types:List<PokemonTypeModel>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertPokemonFavorite(pokemon: PokemonFavorite )
}