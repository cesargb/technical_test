package com.tta.technical_test.data.db

import android.os.Parcelable
import androidx.room.*
import kotlinx.parcelize.Parcelize

@Parcelize
@Entity(tableName = "pokemon", )
data class PokemonDetaiModel (
    @PrimaryKey()
    val id:Int,
    val name: String,
    val url: String,
    val sprite: String,
    val height: Int,
    val weight: Int,
    //val types: List<TypeSlotApiModel>?,
): Parcelable

@Entity(
    tableName = "pokemon_types",
    primaryKeys = ["pokemon_id","name"],
    foreignKeys = [
        ForeignKey(
            entity =  PokemonDetaiModel::class,
            parentColumns = ["id"],
            childColumns = ["pokemon_id"],
            onDelete = ForeignKey.CASCADE),
    ],
)
@Parcelize
data class PokemonTypeModel (
    @ColumnInfo(name = "pokemon_id", index = true)
    val pokemonId:Int,
    val name: String,
    val slot: Int,
): Parcelable
@Entity(
    tableName = "pokemon_favorite",
    primaryKeys = ["pokemon_id"],
    foreignKeys = [
        ForeignKey(
            entity =  PokemonDetaiModel::class,
            parentColumns = ["id"],
            childColumns = ["pokemon_id"],
            onDelete = ForeignKey.NO_ACTION),
    ],
)
@Parcelize
data class PokemonFavorite (

    @ColumnInfo(name = "pokemon_id", index = true)
    val pokemonId:Int,
    val favorite: Boolean,
): Parcelable

@Parcelize
data class PokemonWExtras (
    @Embedded val pokemon:PokemonDetaiModel,
    @Relation(parentColumn = "id",
        entityColumn = "pokemon_id") val favorite:List<PokemonFavorite>?,
    @Relation(parentColumn = "id",
        entityColumn = "pokemon_id") val types:List<PokemonTypeModel>,

   // @Embedded val favorite:PokemonFavorite?,
) : Parcelable {

}