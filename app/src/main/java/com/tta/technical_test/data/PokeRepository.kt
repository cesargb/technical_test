package com.tta.technical_test.data

import com.tta.technical_test.data.net.PokeApi
import javax.inject.Inject
import com.tta.technical_test.common.Result
import com.tta.technical_test.data.di_modules.NetworkCheckModule
import com.tta.technical_test.common.err
import com.tta.technical_test.common.ok
import com.tta.technical_test.data.db.*

interface PokeDataSource {
    suspend fun  getPokemonDetailList(limit:Int,offset:Int): Result<List<PokemonWExtras>>
    suspend fun  setFavoritePokemon(pokemonId:Int,favorite:Boolean): Result<Unit>
    suspend fun  getPokemonById(id:Int): Result<PokemonWExtras?>
}

class PokeCachedRepository @Inject constructor(
    val netData:PokeApi,
    var networkCheckModule: NetworkCheckModule,
    var pokemonDao:PokemonDao,

    ):PokeDataSource{
    override suspend fun getPokemonDetailList(limit: Int, offset: Int): Result<List<PokemonWExtras>> {
       // return err("")
        if(networkCheckModule.isConnected()){
            val list = netData.pokemonList(limit,offset)
            if(!list.isSuccessful) return err("Fallo al hacer petición")
            val details =list.body()?.results!!.map{ pokemon->
                val detail = netData.pokemonDetail(pokemon.name)
                if(!detail.isSuccessful) return err("Fallo al hacer petición")
                detail.body()!!
            }
            val pokemonList = details.map {
                PokemonDetaiModel(id = it.id, name = it.name, url = it.url?:"", sprite = it.sprites.front_default?:"", height = it.height, weight = it.weight)
            }
            val types = details.flatMap { pokemon->
                var id = pokemon.id
                val type = pokemon.types.map { ty-> PokemonTypeModel(pokemonId = pokemon.id, name = ty.type.name, slot = ty.slot) }
                type
            }

            pokemonDao.insertPokemonWithTypes(pokemonList,types)
        }
        val r = pokemonDao.pokemonWithExtrasList(limit,offset)
        return ok(r)
    }
    override suspend fun getPokemonById(id:Int): Result<PokemonWExtras?> {
        val s = pokemonDao.pokemonById(id)
        return ok(s)
    }
    override suspend fun setFavoritePokemon(
        pokemonId: Int,
        favorite: Boolean
    ):Result<Unit> {
        val s = pokemonDao.insertPokemonFavorite(PokemonFavorite(pokemonId,favorite))
        return ok()
    }


}

