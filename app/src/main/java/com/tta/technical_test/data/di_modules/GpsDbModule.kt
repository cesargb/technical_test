package com.tta.technical_test.data.di_modules

import com.google.firebase.firestore.FirebaseFirestore
import com.tta.technical_test.data.net.FireApiService
import com.tta.technical_test.data.net.GpsApi
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class GpsDbModule {
    @Provides
    @Singleton
    fun provideFirestore():FirebaseFirestore = FirebaseFirestore.getInstance()

    @Provides
    @Singleton
    fun provideGpsApi(firestore: FirebaseFirestore):GpsApi = FireApiService(firestore)

}