package com.tta.technical_test.data

import com.squareup.moshi.JsonClass


@JsonClass(generateAdapter = true)
data class PokemonMinApiResult(
    val count: Int?,
    val next: String?,
    val previous: String?,
    val results: List<PokemonMinApiModel>?
)
@JsonClass(generateAdapter = true)
data class PokemonMinApiModel(
    val name: String,
    val url: String
)
@JsonClass(generateAdapter = true)
data class PokemonDetailApiModel(
    val id: Int,
    val name: String,
    val url: String?,
    val sprites: SpritesApiModel,
    val height: Int,
    val weight: Int,
    val types: List<TypeSlotApiModel>,
)
@JsonClass(generateAdapter = true)
data class SpritesApiModel(
    val back_default: String?,
    val back_female: String?,
    val back_shiny: String?,
    val back_shiny_female: String?,
    val front_default: String?,
    val front_female: String?,
    val front_shiny: String?,
    val front_shiny_female: String?,
//    val other: Other,
//    val versions: Versions
)
@JsonClass(generateAdapter = true)
data class TypeSlotApiModel(
    val slot: Int,
    val type: TypeApiModel
)
//
@JsonClass(generateAdapter = true)
data class TypeApiModel(
    val name: String,
    val url: String?
)
