package com.tta.technical_test.data.gps

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.pm.PackageManager
import android.location.Location
import android.os.Looper
import androidx.core.app.ActivityCompat
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationResult
import com.google.android.gms.location.LocationServices
import com.tta.technical_test.common.hasLocationPerms
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.callbackFlow
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class LocationService @Inject constructor(@ApplicationContext val context: Context) {
    companion object {
        const val TIME_INTERVAL:Long =  2*60*1000
    }
    private val fusedLocationClient = LocationServices.getFusedLocationProviderClient(context)
    private val locationRequest: LocationRequest = LocationRequest.Builder(
        TIME_INTERVAL
        //1000*5
    ).build()
    private var locationCallback: LocationCallback? = null

    @SuppressLint("MissingPermission")
    fun getGpsUpdates() = callbackFlow<GpsLocation>{
        if (!context.hasLocationPerms()) {
            close(Throwable("No se tienen permisos suficientes"))
            return@callbackFlow
        }
        
        locationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult) {
                locationResult.locations.forEach{
                    trySend(GpsLocation(lon = it.longitude, lat = it.latitude,System.currentTimeMillis()))
                }
            }
        }
        fusedLocationClient.lastLocation
            .addOnSuccessListener { location: Location? ->
                location?.let {
                    trySend(GpsLocation(lon = it.longitude, lat = it.latitude,System.currentTimeMillis()))
                }
            }
        fusedLocationClient
            .requestLocationUpdates(locationRequest, locationCallback!!, Looper.getMainLooper())
        awaitClose { fusedLocationClient.removeLocationUpdates(locationCallback!!) }
    }
    fun stopGpsUpdates(){
        if(locationCallback==null) return
        fusedLocationClient.removeLocationUpdates(locationCallback!!)
    }


}

data class GpsLocation(val lon:Double,val lat:Double,val time:Long)