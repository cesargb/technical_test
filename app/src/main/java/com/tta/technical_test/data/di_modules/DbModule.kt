package com.tta.technical_test.data.di_modules

import android.content.Context
import androidx.room.Room
import com.tta.technical_test.data.db.PokemonDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton


@InstallIn(SingletonComponent::class)
@Module
class DbModule {
    private val  DATABASE_NAME:String = "pokemon_database"

    @Singleton
    @Provides
    fun provideRoom(@ApplicationContext context: Context): PokemonDatabase {
        return Room
            .databaseBuilder(context,PokemonDatabase::class.java,DATABASE_NAME)
            //.fallbackToDestructiveMigration()

            .build()
    }
    @Singleton
    @Provides
    fun providePokemonDao(db:PokemonDatabase) = db.getPokemonDao()
}