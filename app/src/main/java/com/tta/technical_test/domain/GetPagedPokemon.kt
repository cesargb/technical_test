package com.tta.technical_test.domain

import com.tta.technical_test.common.Result
import com.tta.technical_test.data.PokeCachedRepository
import com.tta.technical_test.data.db.PokemonWExtras
import javax.inject.Inject

class GetPagedPokemon @Inject constructor(
    val repository: PokeCachedRepository
) {
    suspend operator fun invoke(limit: Int =25, offset: Int =0): Result<List<PokemonWExtras>> {
        return repository.getPokemonDetailList(limit,offset)
    }
}