package com.tta.technical_test.domain
import com.tta.technical_test.data.GPSRepository
import javax.inject.Inject

class StopGPS @Inject constructor(
    private val locationRepository: GPSRepository
) {
    suspend operator fun invoke(){
        locationRepository.startGetUpdates()
    }
}