package com.tta.technical_test.domain

import com.tta.technical_test.common.Result
import com.tta.technical_test.data.PokeCachedRepository
import com.tta.technical_test.data.db.PokemonWExtras
import javax.inject.Inject

class GetPokemon @Inject constructor(
    val repository: PokeCachedRepository
) {
    suspend operator fun invoke(id:Int): Result<PokemonWExtras?> {
        return repository.getPokemonById(id)
    }
}