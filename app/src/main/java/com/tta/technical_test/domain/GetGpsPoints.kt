package com.tta.technical_test.domain

import com.tta.technical_test.data.GPSRepository
import com.tta.technical_test.data.gps.GpsLocation
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetGpsPoints @Inject constructor(
    private val locationRepository: GPSRepository
) {
    suspend operator fun invoke(): Flow<List<GpsLocation>>  {
        return  locationRepository.getGpsPoints()

    }
}