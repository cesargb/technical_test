package com.tta.technical_test.domain

import android.util.Log
import com.tta.technical_test.data.GPSRepository
import javax.inject.Inject

class StartGPS @Inject constructor(
    private val locationRepository: GPSRepository
) {
    suspend operator fun invoke() {
        locationRepository.startGetUpdates().collect{
            Log.d("GPS<<<<<<<<<<<<",it.toString())
            locationRepository.saveGpsPoint(point = it)
        }

    }
}