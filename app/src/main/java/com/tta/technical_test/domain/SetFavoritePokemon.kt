package com.tta.technical_test.domain

import com.tta.technical_test.common.Result
import com.tta.technical_test.data.PokeCachedRepository
import com.tta.technical_test.data.db.PokemonWExtras
import javax.inject.Inject

class SetFavoritePokemon @Inject constructor(
    val repository: PokeCachedRepository
) {
    suspend operator fun invoke(pokemonId:Int,favorite: Boolean): Result<Unit> {
        return repository.setFavoritePokemon(pokemonId, favorite)
    }
}