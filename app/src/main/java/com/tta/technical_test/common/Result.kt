package com.tta.technical_test.common

data class Result<T>(
    val data:T?,
    val ok:Boolean,
    val msg:String?) {

}

fun<T> ok(data:T):Result<T> = Result(data,true,null)
fun ok():Result<Unit> = Result(Unit,true,null)
fun<T> err(msg:String):Result<T> = Result(null,false,msg)
