package com.tta.technical_test.ui.views

import android.Manifest.permission.ACCESS_COARSE_LOCATION
import android.Manifest.permission.ACCESS_FINE_LOCATION
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.result.contract.ActivityResultContracts
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.tta.technical_test.common.hasLocationPerms
import com.tta.technical_test.databinding.FragmentMapBinding
import com.tta.technical_test.ui.viewmodels.MapViewModel
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class MapFragment : Fragment() {

    companion object {
        fun newInstance() = MapFragment()
    }
    private var _binding: FragmentMapBinding? = null
    private val binding get() = _binding!!
    private val viewModel: MapViewModel by viewModels()

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentMapBinding.inflate(inflater, container, false)
        if(!requireContext().hasLocationPerms()){

            registerForActivityResult(
                ActivityResultContracts.RequestMultiplePermissions()
                ) { permissions ->
                    viewModel.onPermChange(permissions.entries.all { it.value })
            }.launch(arrayOf(
                ACCESS_COARSE_LOCATION,
                ACCESS_FINE_LOCATION
            ))


        }else{
            viewModel.onPermChange(true)
        }

        loadViews()
        loc()
        return binding.root
    }

    private fun loc() {

    }

    private fun loadViews() {
        val maps =
            childFragmentManager.findFragmentByTag("mapFragment") as SupportMapFragment?
        maps?.getMapAsync{ map->
            viewModel.list.observe(viewLifecycleOwner){
                map.clear()
                if(it.isNotEmpty()) {
                    map.animateCamera(
                        CameraUpdateFactory.newLatLngZoom(
                            LatLng(it.last().lat, it.last().lon),
                            14F
                        )
                    )
                }
                it.forEach { loc ->
                    map.addMarker(MarkerOptions()
                        .position(LatLng(loc.lat,loc.lon))
                        .title("Lat: ${loc.lat} Lon:${loc.lon}"))
                }


            }
            viewModel.loadPoints()
        }
        binding.btnGps.setOnClickListener {
            viewModel.onGpsClick()
        }
        binding.btnHistory.setOnClickListener {
            val intent = Intent(requireContext(), PointListActivity::class.java)
            requireContext().startActivity(intent)
        }
        viewModel.withPerm.observe(viewLifecycleOwner){
            binding.btnGps.isEnabled=it?:false
        }
        viewModel.gps.observe(viewLifecycleOwner){
            binding.btnGps.text = if(it) "Apagar seguimiento" else "Iniciar seguimiento"
        }




    }


}

