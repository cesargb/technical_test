package com.tta.technical_test.ui.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.tta.technical_test.ui.views.MainFragment
import com.tta.technical_test.ui.views.MapFragment

class TabAdapter(activity:FragmentActivity,private val tabCount:Int): FragmentStateAdapter(activity) {
    override fun getItemCount(): Int {
        return  tabCount
    }

    override fun createFragment(position: Int): Fragment {
        return when(position)
        {
            0->MainFragment()
            1->MapFragment()
            else -> MainFragment()
        }
    }

}