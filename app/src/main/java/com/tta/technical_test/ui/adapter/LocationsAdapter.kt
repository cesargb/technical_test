package com.tta.technical_test.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.tta.technical_test.R
import com.tta.technical_test.data.gps.GpsLocation
import com.tta.technical_test.databinding.LocationItemBinding
import java.text.SimpleDateFormat
import java.util.*

class LocationsAdapter(
    var list:List<GpsLocation>
) :
    RecyclerView.Adapter<LocationsAdapter.ViewHolder>() {

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

         private val binding = LocationItemBinding.bind(view)

        fun bindItems(data: GpsLocation) {
            //binding. = data.name
            binding.txtLat.text = "Lat: ${data.lat}"
            binding.txtLog.text  = "Lon: ${data.lon}"
            binding.txtDate.text  = "Fecha: ${getDate(data.time,"dd/MM/yyyy hh:mm")}"
        }
        private fun getDate(milliSeconds: Long, dateFormat: String?): String? {
            val formatter = SimpleDateFormat(dateFormat,Locale.US)
            val calendar: Calendar = Calendar.getInstance()
            calendar.timeInMillis = milliSeconds
            return formatter.format(calendar.time)
        }
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        // Create a new view, which defines the UI of the list item
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.location_item, viewGroup, false)

        return ViewHolder(view)
    }


    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        val data = list[position]
        viewHolder.bindItems(data)
    }

    override fun getItemCount() = list.size

    fun setData(l:List<GpsLocation>){
        //list.clear()
        list = l//data.toMutableList()
        notifyDataSetChanged()
    }


}