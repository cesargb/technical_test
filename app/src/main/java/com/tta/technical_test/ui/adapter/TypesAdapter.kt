package com.tta.technical_test.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.tta.technical_test.R
import com.tta.technical_test.data.db.PokemonTypeModel
import com.tta.technical_test.data.db.PokemonWExtras
import com.tta.technical_test.data.gps.GpsLocation
import com.tta.technical_test.databinding.PokemonListItemBinding
import com.tta.technical_test.databinding.SimpleItemBinding

class TypesAdapter(
    private var list:List<PokemonTypeModel>
) :
    RecyclerView.Adapter<TypesAdapter.ViewHolder>() {

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

         private val binding = SimpleItemBinding.bind(view)

        fun bindItems(data: PokemonTypeModel) {
            binding.text.text = data.name
        }
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        // Create a new view, which defines the UI of the list item
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.simple_item, viewGroup, false)

        return ViewHolder(view)
    }


    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        val data = list[position]
        viewHolder.bindItems(data)
    }

    override fun getItemCount() = list.size
    fun setData(l:List<PokemonTypeModel>){

        list = l
        notifyDataSetChanged()
    }


}