package com.tta.technical_test.ui.views

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.OnScrollListener
import com.bumptech.glide.Glide
import com.tta.technical_test.common.EndlessRecyclerViewScrollListener
import com.tta.technical_test.data.db.PokemonWExtras
import com.tta.technical_test.databinding.FragmentMainBinding
import com.tta.technical_test.ui.adapter.MainListAdapter
import com.tta.technical_test.ui.viewmodels.MainViewModel
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class MainFragment : Fragment() {

    companion object {
        fun newInstance() = MainFragment()
    }

    private var _binding: FragmentMainBinding? = null
    private val binding get() = _binding!!
    private val viewModel: MainViewModel by viewModels()
    private lateinit var adapter: MainListAdapter
    private var scrollListener: EndlessRecyclerViewScrollListener? = null
    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if(savedInstanceState==null){
            viewModel.loadMore()
        }
        adapter = MainListAdapter({ toDetail(it)},
            {viewModel.onItemImageClick(it)},
            {id,fav-> viewModel.onFavoriteClick(id,fav)}
        )
    }



    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentMainBinding.inflate(inflater, container, false)
        loadRV()
        loadViews()
        return binding.root
    }
    private fun loadRV(){
        binding.rv.adapter = adapter
        val layout = LinearLayoutManager(this.context)
        binding.rv.layoutManager = layout
        scrollListener = object : EndlessRecyclerViewScrollListener(layout) {

            override fun onLoadMore(page: Int, totalItemsCount: Int, view: RecyclerView?) {
                viewModel.loadMore()
            }
        }
        binding.rv.addOnScrollListener(scrollListener!!)

    }

    private fun loadViews(){
        viewModel.list.observe(viewLifecycleOwner) {
            Log.d("por aqui <<<<<<<<<<<<<<<<<<<<<", "aaaaaaaaaaaaaaaaaaaaaaaaaa")
            adapter.updateData(it)
        }
        binding.fullImage.setOnClickListener { viewModel.onItemImageClick(null)}
        viewModel.showFull.observe(viewLifecycleOwner){
            if(it==null){
                binding.fullImage.visibility = View.GONE
            }else{
                Glide.with(this)
                    .load(it)
                    .into(binding.fullImage)
                binding.fullImage.visibility = View.VISIBLE
            }
        }
    }
    private fun toDetail(it: PokemonWExtras) {
        val intent = Intent(requireContext(), DetailActivity::class.java)
        intent.putExtra("PokemonWExtras",it)

        requireContext().startActivity(intent)

    }
//    var resultLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
//        if (result.resultCode == Activity.RESULT_OK) {
//            // There are no request codes
//            val data: Intent? = result.data
//            val id = data?.extras?.getInt("pokemon_id")
//
//        }
//    }



}