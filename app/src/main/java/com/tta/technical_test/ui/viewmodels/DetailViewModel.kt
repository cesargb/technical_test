package com.tta.technical_test.ui.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.tta.technical_test.data.db.PokemonWExtras
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class DetailViewModel @Inject constructor() : ViewModel() {
    private val _detail = MutableLiveData<PokemonWExtras>();
    val detail: LiveData<PokemonWExtras> = _detail

    fun onCreated(d:PokemonWExtras ){
        _detail.value = d
    }
}