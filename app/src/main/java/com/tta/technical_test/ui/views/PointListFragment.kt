package com.tta.technical_test.ui.views

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.tta.technical_test.databinding.FragmentPointlistBinding
import com.tta.technical_test.ui.adapter.LocationsAdapter
import com.tta.technical_test.ui.viewmodels.PointListViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class PointListFragment : Fragment() {

    companion object {
        fun newInstance() = PointListFragment()
    }
    private var _binding: FragmentPointlistBinding? = null
    private val binding get() = _binding!!
    private lateinit var adapter: LocationsAdapter
    private  val viewModel: PointListViewModel by viewModels()
    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentPointlistBinding.inflate(inflater, container, false)

        viewModel.onCreated()

        rv()
        conections()
        return binding.root
    }
    private fun rv(){
        adapter = LocationsAdapter(listOf())
        binding.rv.adapter = adapter
        binding.rv.layoutManager = LinearLayoutManager(this.context)
    }
    private fun conections(){
        viewModel.list.observe(viewLifecycleOwner){
            adapter.setData(it)
        }


    }
}