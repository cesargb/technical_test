package com.tta.technical_test.ui.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.tta.technical_test.data.db.PokemonFavorite
import com.tta.technical_test.data.db.PokemonWExtras
import com.tta.technical_test.domain.GetPagedPokemon
import com.tta.technical_test.domain.GetPokemon
import com.tta.technical_test.domain.SetFavoritePokemon
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject() constructor(
    private val getPagedPokemon:GetPagedPokemon,
    private val setFavoritePokemon:SetFavoritePokemon,
    private val getPokemon: GetPokemon,
) : ViewModel() {

//    private val _state = MutableLiveData<ViewState>();
//    val state: LiveData<ViewState> = _state

//    private val  = MutableLiveData<String>();
//    val userInput: LiveData<String> = _userInput
//
//    private val _resultText = MutableLiveData<String>();
//    val resultText: LiveData<String> = _resultText
    private val _list = MutableLiveData<MutableList<PokemonWExtras>>(mutableListOf());
    val list: LiveData<MutableList<PokemonWExtras>> = _list
    private val _showFull = MutableLiveData<String?>(null);
    val showFull: LiveData<String?> = _showFull

    var  loading=false;


    fun loadMore(){
        viewModelScope.launch {
            if(loading) return@launch
            loading= true
            val pokemon = withContext(Dispatchers.IO){
                getPagedPokemon(25,_list.value?.size?:0)
            }
            if(!pokemon.ok){
                return@launch
            }

            _list.value = _list.value!!.plus(pokemon.data!!).toMutableList()
            loading= false
        }

    }
    fun onItemImageClick(data:PokemonWExtras?){
        _showFull.value = data?.pokemon?.sprite
    }
    fun updatePokemon(id:Int){
        viewModelScope.launch{
            val p = withContext(Dispatchers.IO) {getPokemon(id)}
            if(p.ok && p.data!=null){
                val i = _list.value!!.indexOfFirst { it.pokemon.id==id}
                if(i>=0 ){
                    _list.value!![i] = p.data
                    _list.value = _list.value
                }
            }
        }
    }

    fun onFavoriteClick(pokemon_id:Int,favorite:Boolean){
        viewModelScope.launch {
            val i = _list.value!!.indexOfFirst { it.pokemon.id==pokemon_id}

            if(i>=0 ){
                _list.value!![i] =  _list.value!![i]
                    .copy(favorite = listOf(PokemonFavorite(pokemon_id,favorite)))
                _list.value = _list.value
            }

            withContext(Dispatchers.IO){
                setFavoritePokemon(pokemon_id,favorite)
            }
        }

    }


}