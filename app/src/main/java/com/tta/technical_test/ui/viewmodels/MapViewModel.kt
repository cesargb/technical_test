package com.tta.technical_test.ui.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.tta.technical_test.data.gps.GpsLocation
import com.tta.technical_test.domain.GetGpsPoints
import com.tta.technical_test.domain.StartGPS
import com.tta.technical_test.domain.StopGPS
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class MapViewModel @Inject() constructor(
    private val startGPS: StartGPS,
    private val stopGPS: StopGPS,
    private val getGpsPoints: GetGpsPoints,
) : ViewModel () {

    private val _withPerm = MutableLiveData(false);
    val withPerm: LiveData<Boolean> = _withPerm

    private val _gps = MutableLiveData(false);
    val gps: LiveData<Boolean> = _gps
    private val _list = MutableLiveData<List<GpsLocation>>(mutableListOf());
    val list: LiveData<List<GpsLocation>> = _list

    fun loadPoints(){
        viewModelScope.launch {
            withContext(Dispatchers.IO){
                getGpsPoints().collect{
                    _list.postValue(it)
                }
            }
        }
    }
    fun onPermChange(v:Boolean){
        _withPerm.value = v
    }

    fun onGpsClick(){
        viewModelScope.launch {
            withContext(Dispatchers.IO){
                if(_gps.value == true){
                    _gps.postValue(!_gps.value!!)
                    stopGPS()
                }else{
                    _gps.postValue(!_gps.value!!)
                    startGPS()
                }
            }

        }
    }

}