package com.tta.technical_test.ui.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.tta.technical_test.data.db.PokemonWExtras
import com.tta.technical_test.data.gps.GpsLocation
import com.tta.technical_test.domain.GetGpsPoints
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class PointListViewModel @Inject constructor(
    private val getGpsPoints: GetGpsPoints
) : ViewModel() {
    private val _list = MutableLiveData<List<GpsLocation>>(mutableListOf());
    val list: LiveData<List<GpsLocation>> = _list
    fun onCreated(){
        viewModelScope.launch {
            withContext(Dispatchers.IO){
                getGpsPoints().collect{
                    _list.postValue(it)
                }
            }
        }

    }

}