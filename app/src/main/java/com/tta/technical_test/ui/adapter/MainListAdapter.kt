package com.tta.technical_test.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.tta.technical_test.R
import com.tta.technical_test.data.db.PokemonWExtras
import com.tta.technical_test.databinding.PokemonListItemBinding

class MainListAdapter(
    val onClick:(PokemonWExtras)->Unit,
    val onImageClick:(PokemonWExtras)->Unit,
    val onFavoriteClick:(pokemon_id:Int,favorite:Boolean)->Unit,
) :
    RecyclerView.Adapter<MainListAdapter.ViewHolder>() {

    private var dataSet: MutableList<PokemonWExtras> = mutableListOf()
    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

         private val binding = PokemonListItemBinding.bind(view)

        fun bindItems(data: PokemonWExtras,
                      onClick:(PokemonWExtras)->Unit,
                      onImageClick:(PokemonWExtras)->Unit,
                      onFavoriteClick:(pokemon_id:Int,favorite:Boolean)->Unit,
        ) {
            binding.txtName.text = data.pokemon.name
            binding.circularImage.load(data.pokemon.sprite,data.pokemon.name)
            binding.circularImage.setOnClickListener { onImageClick(data) }
            binding.txtName.setOnClickListener { onClick(data) }
            binding.checkFav.isChecked = data.favorite?.getOrNull(0)?.favorite?:false
            binding.checkFav.setOnClickListener {
                onFavoriteClick(data.pokemon.id,binding.checkFav.isChecked?:false)
            }
        }
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        // Create a new view, which defines the UI of the list item
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.pokemon_list_item, viewGroup, false)

        return ViewHolder(view)
    }


    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        val data = dataSet[position]
        viewHolder.bindItems(data,onClick,onImageClick,onFavoriteClick)
    }

    override fun getItemCount() = dataSet.size

    fun updateData(data: List<PokemonWExtras>){
        dataSet.clear()
        dataSet = data.toMutableList()
        notifyDataSetChanged()
    }


}