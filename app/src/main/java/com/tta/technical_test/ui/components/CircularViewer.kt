package com.tta.technical_test.ui.components

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.drawable.Drawable
import android.text.TextPaint
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.withStyledAttributes
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.tta.technical_test.R
import com.tta.technical_test.databinding.CircularViewerBinding


/**
 * TODO: document your custom view class.
 */
class CircularViewer(
    context: Context,
    attrs: AttributeSet
) : CardView(context,attrs) {
    private val binding = CircularViewerBinding.inflate(
        LayoutInflater.from(context),
        this,
        true)

    private var url = ""
    private var text = ""
    private  var defaultPlaceholder:Int = 0

    init {
        context.withStyledAttributes(attrs,R.styleable.CircularViewer){
            //text = getString(R.styleable.CircularViewer_text) ?: ""
            binding.resultText.text = text

            if(hasValue(R.styleable.CircularViewer_textColor)){
                binding.resultText.setTextColor(getColor(getResourceId(R.styleable.CircularViewer_textColor,0),0))
            }
            if(hasValue(R.styleable.CircularViewer_viewBackground)){
                binding.resultText.setBackgroundResource(getResourceId(R.styleable.CircularViewer_viewBackground,0))
                binding.mainImage.setBackgroundResource(getResourceId(R.styleable.CircularViewer_viewBackground,0))
            }
            if(hasValue(R.styleable.CircularViewer_placeholder)){
                defaultPlaceholder = getResourceId(R.styleable.CircularViewer_placeholder,R.drawable.placeholder)
                binding.mainImage.setImageResource(defaultPlaceholder)
            }
        }



    }
    fun load(url:String,text:String?){


        Glide.with(this)
            .load(url)
            .addListener(object :RequestListener<Drawable>{
                override fun onLoadFailed(
                    e: GlideException?,
                    model: Any?,
                    target: Target<Drawable>?,
                    isFirstResource: Boolean
                ): Boolean {
                    if(text.isNullOrEmpty()|| !text[0].isLetter()){
                        binding.mainImage.setImageResource(defaultPlaceholder)
                        return false
                    }
                    val ini = text
                        .split("\\s+".toRegex() )
                        .take(2)
                        .map { it.first().uppercaseChar() }
                        .joinToString()
                    if(ini.isEmpty()){
                        binding.mainImage.setImageResource(defaultPlaceholder)
                        return false
                    }
                    binding.resultText.text = ini
                    binding.mainImage.visibility = View.GONE
                    return false;
                }

                override fun onResourceReady(
                    resource: Drawable?,
                    model: Any?,
                    target: Target<Drawable>?,
                    dataSource: DataSource?,
                    isFirstResource: Boolean
                ): Boolean {
                    return false;
                }

            })
            .into(binding.mainImage)

    }




}