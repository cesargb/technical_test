package com.tta.technical_test.ui.views

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.tta.technical_test.common.parcelable
import com.tta.technical_test.data.db.PokemonWExtras
import com.tta.technical_test.databinding.FragmentDetailBinding
import com.tta.technical_test.ui.adapter.TypesAdapter
import com.tta.technical_test.ui.viewmodels.DetailViewModel

class DetailFragment : Fragment() {

    companion object {
        fun newInstance(extras: Bundle?) = DetailFragment().apply {
            arguments = extras
        }
    }
    private var _binding: FragmentDetailBinding? = null
    private val binding get() = _binding!!

    private  val viewModel: DetailViewModel by viewModels()
    private lateinit var adapter: TypesAdapter
    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if(savedInstanceState==null){

        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        _binding = FragmentDetailBinding.inflate(inflater, container, false)
        val p = arguments?.parcelable<PokemonWExtras>("PokemonWExtras")!!
        viewModel.onCreated(p)
        rv(p)
        connections()
        return binding.root
    }
    private fun rv(p:PokemonWExtras){
        adapter = TypesAdapter(p.types)
        binding.rv.adapter = adapter
        binding.rv.layoutManager = LinearLayoutManager(this.context)
    }
    private fun connections(){
        viewModel.detail.observe(viewLifecycleOwner){
           binding.checkFav.isChecked = it.favorite?.getOrNull(0)?.favorite?:false
           binding.txtName.text = "${it.pokemon.name}"
           binding.txtWeight.text = "Pesa: ${it.pokemon.weight}Kg"
            binding.txtHeight.text = "Mide: ${it.pokemon.height}M"
            binding.mainImage.load(it.pokemon.sprite,it.pokemon.name)
            adapter.setData(it.types)
        }
    }

}